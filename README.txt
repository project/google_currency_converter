CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module will help your website convert currency using the Google finance
calculator. It will give you a block to convert currency from one format to
another. This module also provides the option to set your default block
currency.

INSTALLATION
------------
* Install as usual,
see https://www.drupal.org/documentation/install/modules-themes/modules-8.

CONFIGURATION
-------------
* Enable Google Currency Converter block, at
   admin » structure » block layout

MAINTAINERS
-----------
Current maintainers:
 * Naveen Valecha (naveenvalecha) - https://drupal.org/u/naveenvalecha
